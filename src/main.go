package main

import (
	"./service"
	"fmt"
	"github.com/faiface/pixel/pixelgl"
	"os"
)

const (
	ConfigurationFilePath = "conf.json"
)

func run() {
	if configuration, err := service.BuildConfiguration(ConfigurationFilePath); err == nil {
		if srv, err := service.BuildService(configuration); err == nil {
			for srv.IsRunning() {
				srv.Update()
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	pixelgl.Run(run)
}
