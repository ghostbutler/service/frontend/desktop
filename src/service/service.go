package service

import (
	"./data"
	"./directory"
	_ "./statik"
	"./window"
	"github.com/rakyll/statik/fs"
	"net/http"
)

type Service struct {
	// configuration
	configuration *Configuration

	// directory handler
	directory *directory.Directory

	// data
	directoryData data.Directory

	// window
	window *window.Window

	// statik file system
	statikFS http.FileSystem
}

// build service
func BuildService(configuration *Configuration) (*Service, error) {
	service := &Service{
		configuration: configuration,
	}

	if f, err := fs.New(); err == nil {
		service.statikFS = f
	} else {
		return nil, err
	}

	service.directory = directory.BuildDirectory(&configuration.Directory,
		&service.directoryData)

	if w, err := window.BuildWindow(&service.directoryData,
		service.statikFS); err == nil {
		service.window = w
	} else {
		return nil, err
	}

	return service, nil
}

// update
func (service *Service) Update() {
	// update window
	service.window.Update()

	// must test a device ?
	if cell := service.window.GetTestDevice(); cell != nil {
		go directory.TestDevice(&service.configuration.Directory,
			cell)
	}

	// must send an action?
	if action := service.window.GetAction(); action != nil {
		go directory.SendAction(&service.configuration.Directory,
			action)
	}
}

func (service *Service) IsRunning() bool {
	return service.window.IsRunning
}
