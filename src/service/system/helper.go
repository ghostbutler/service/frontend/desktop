package system

import (
	"io"
	"os"
)

// close reader with error handling
func CloseReader(file io.ReadCloser) {
	if err := file.Close(); err != nil {
		_, _ = os.Stderr.WriteString(err.Error())
	}
}

// close writer with error handling
func CloseWriter(file io.WriteCloser) {
	if err := file.Close(); err != nil {
		_, _ = os.Stderr.WriteString(err.Error())
	}
}

// does the file exist?
func IsFileExist(filePath string) bool {
	_, err := os.Stat(filePath)
	if err == nil {
		return true
	} else {
		return false
	}
}
