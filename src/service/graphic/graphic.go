package graphic

import (
	"../system"
	"github.com/faiface/pixel"
	"image"
	_ "image/png"
	"io"
	"os"
)

// load png image
func LoadImageFromReader(reader io.Reader) (pixel.Picture, error) {
	if img, _, err := image.Decode(reader); err == nil {
		return pixel.PictureDataFromImage(img), nil
	} else {
		return nil, err
	}
}

// load png image
func LoadImage(imagePath string) (pixel.Picture, error) {
	if file, err := os.Open(imagePath); err == nil {
		defer system.CloseReader(file)
		return LoadImageFromReader(file)
	} else {
		return nil, err
	}
}

// load application icon
func LoadApplicationIcon(iconPath string) ([]pixel.Picture, error) {
	if icon, err := LoadImage(iconPath); err == nil {
		return []pixel.Picture{icon}, nil
	} else {
		return nil, err
	}
}
