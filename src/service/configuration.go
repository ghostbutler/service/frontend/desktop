package service

import (
	"./directory"
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	Directory directory.Configuration `json:"directory"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	var configuration Configuration
	if content, err := ioutil.ReadFile(configurationFilePath); err == nil {
		if err := json.Unmarshal(content,
			&configuration); err == nil {
			return &configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
