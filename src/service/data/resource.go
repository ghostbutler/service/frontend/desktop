package data

import (
	"../graphic"
	"../system"
	"github.com/faiface/pixel"
	"net/http"
)

const (
	DeviceIconWidth  = 64
	DeviceIconHeight = 64

	PowerImageWidth  = 64
	PowerImageHeight = 64

	DeviceIconPath         = "iconSheet.png"
	BrightnessImagePath    = "brightness.png"
	HueSaturationImagePath = "hueSaturation.png"
	PowerImagePath         = "power.png"
	MouseImagePath         = "mouse.png"
)

var DeviceIconPositionMap = map[Capability]int{
	CapabilityGamepadButton:   1,
	CapabilityGamepadJoystick: 5,
	CapabilityLight:           4,
	CapabilityOutlet:          3,
	CapabilityMusic:           2,
	CapabilityTTS:             0,
}

type Resource struct {
	DeviceIcon        map[Capability]map[bool]*pixel.Sprite
	DeviceIconPicture pixel.Picture

	HueSaturation *pixel.Sprite
	Brightness    *pixel.Sprite

	Power map[bool]*pixel.Sprite

	Mouse *pixel.Sprite
}

// build resources
func BuildResource(statikFS http.FileSystem) (*Resource, error) {
	resource := &Resource{
		DeviceIcon: make(map[Capability]map[bool]*pixel.Sprite),
		Power:      make(map[bool]*pixel.Sprite),
	}
	if file, err := statikFS.Open("/" +
		DeviceIconPath); err == nil {
		defer system.CloseReader(file)
		if picture, err := graphic.LoadImageFromReader(file); err == nil {
			resource.DeviceIconPicture = picture
			for capability, horizontalIndex := range DeviceIconPositionMap {
				resource.DeviceIcon[capability] = make(map[bool]*pixel.Sprite)
				resource.DeviceIcon[capability][false] = pixel.NewSprite(picture,
					pixel.R(float64(horizontalIndex*DeviceIconWidth),
						DeviceIconHeight,
						float64((horizontalIndex+1)*DeviceIconWidth),
						picture.Bounds().Max.Y))
				resource.DeviceIcon[capability][true] = pixel.NewSprite(picture,
					pixel.R(float64(horizontalIndex*DeviceIconWidth),
						0,
						float64((horizontalIndex+1)*DeviceIconWidth),
						DeviceIconHeight))
			}
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
	if file, err := statikFS.Open("/" +
		BrightnessImagePath); err == nil {
		defer system.CloseReader(file)
		if picture, err := graphic.LoadImageFromReader(file); err == nil {
			resource.Brightness = pixel.NewSprite(picture,
				picture.Bounds())
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
	if file, err := statikFS.Open("/" +
		HueSaturationImagePath); err == nil {
		defer system.CloseReader(file)
		if picture, err := graphic.LoadImageFromReader(file); err == nil {
			resource.HueSaturation = pixel.NewSprite(picture,
				picture.Bounds())
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
	if file, err := statikFS.Open("/" +
		PowerImagePath); err == nil {
		defer system.CloseReader(file)
		if picture, err := graphic.LoadImageFromReader(file); err == nil {
			resource.Power[false] = pixel.NewSprite(picture,
				pixel.R(0,
					PowerImageHeight,
					PowerImageWidth,
					picture.Bounds().Max.Y))
			resource.Power[true] = pixel.NewSprite(picture,
				pixel.R(0,
					0,
					PowerImageWidth,
					PowerImageHeight))
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
	if file, err := statikFS.Open("/" +
		MouseImagePath); err == nil {
		defer system.CloseReader(file)
		if picture, err := graphic.LoadImageFromReader(file); err == nil {
			resource.Mouse = pixel.NewSprite(picture,
				picture.Bounds())
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
	return resource, nil
}
