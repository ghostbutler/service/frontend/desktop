package data

import (
	"github.com/faiface/pixel"
	"gitlab.com/ghostbutler/tool/service/device"
	"sync"
)

type Directory struct {
	// capability list
	CapabilityList []device.CapabilityType

	// device name list
	DeviceList map[device.CapabilityType][]string

	// active devices list
	ActiveDeviceList map[device.CapabilityType]map[string]interface{}

	sync.Mutex
}

func (directory *Directory) IsDeviceActive(capabilityType device.CapabilityType,
	name string) bool {
	if deviceList, ok := directory.ActiveDeviceList[capabilityType]; ok {
		if _, ok := deviceList[name]; ok {
			return true
		}
	}
	return false
}

// device cell
type DeviceCell struct {
	IsActive bool
	Name     string
	Rect     pixel.Rect
	Position pixel.Vec
	Type     device.CapabilityType
}
