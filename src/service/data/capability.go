package data

import (
	"gitlab.com/ghostbutler/tool/service/device"
)

type Capability string

const (
	CapabilityGamepadButton   = Capability("button")
	CapabilityGamepadJoystick = Capability("joystick")
	CapabilityLight           = Capability("light")
	CapabilityOutlet          = Capability("outlet")
	CapabilityMusic           = Capability("music")
	CapabilityTTS             = Capability("tts")
)

var CapabilityList = []Capability{
	CapabilityGamepadButton,
	CapabilityGamepadJoystick,
	CapabilityLight,
	CapabilityOutlet,
	CapabilityMusic,
	CapabilityTTS,
}

var CapabilityMap = map[Capability]device.CapabilityType{
	CapabilityGamepadButton:   device.CapabilityTypeGamepad,
	CapabilityGamepadJoystick: device.CapabilityTypeGamepad,
	CapabilityLight:           device.CapabilityTypeLight,
	CapabilityOutlet:          device.CapabilityTypeOutlet,
	CapabilityMusic:           device.CapabilityTypeMusic,
	CapabilityTTS:             device.CapabilityTypeTextToSpeech,
}
