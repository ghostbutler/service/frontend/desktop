package directory

import (
	"../data"
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	DelayBetweenDirectoryUpdate = time.Second * 1
)

type Directory struct {
	isRunning bool

	// hostname
	hostname string

	// data
	data *data.Directory
}

func BuildDirectory(configuration *Configuration,
	data *data.Directory) *Directory {
	directory := &Directory{
		data:      data,
		isRunning: true,
		hostname:  configuration.Hostname,
	}
	go directory.update()
	return directory
}

func (directory *Directory) requestKnownType() ([]device.CapabilityType, error) {
	request, _ := http.NewRequest("GET",
		"https://"+
			directory.hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/types",
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		defer common.Close(response.Body)
		var capabilityList []string
		content, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(content,
			&capabilityList); err == nil {
			var output []device.CapabilityType
			for _, capability := range capabilityList {
				output = append(output,
					device.FindCapabilityFromName(capability))
			}
			return output, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (directory *Directory) requestDeviceForType(capabilityType device.CapabilityType) ([]string, error) {
	request, _ := http.NewRequest("GET",
		"https://"+
			directory.hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/list?type="+
			device.CapabilityTypeName[capabilityType],
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		defer common.Close(response.Body)
		var result []string
		content, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(content,
			&result); err == nil {
			return result, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (directory *Directory) requestActiveDevice() ([]device.ActiveSensor, error) {
	request, _ := http.NewRequest("GET",
		"https://"+
			directory.hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/active",
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		defer common.Close(response.Body)
		var result []device.ActiveSensor
		content, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(content,
			&result); err == nil {
			return result, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (directory *Directory) update() {
	for directory.isRunning {
		if capabilityList, err := directory.requestKnownType(); err == nil {
			newDeviceList := make(map[device.CapabilityType][]string)
			for _, capability := range capabilityList {
				if deviceList, err := directory.requestDeviceForType(capability); err == nil {
					sort.Strings(deviceList)
					newDeviceList[capability] = deviceList
				}
			}
			finalActiveDeviceList := make(map[device.CapabilityType]map[string]interface{})
			if activeDeviceList, err := directory.requestActiveDevice(); err == nil {
				for _, activeDevice := range activeDeviceList {
					if _, ok := finalActiveDeviceList[device.FindCapabilityFromName(activeDevice.CapabilityType)]; !ok {
						finalActiveDeviceList[device.FindCapabilityFromName(activeDevice.CapabilityType)] = make(map[string]interface{})
					}
					finalActiveDeviceList[device.FindCapabilityFromName(activeDevice.CapabilityType)][activeDevice.Name] = nil
				}
			}

			directory.data.Lock()
			directory.data.CapabilityList = capabilityList
			directory.data.DeviceList = newDeviceList
			directory.data.ActiveDeviceList = finalActiveDeviceList
			directory.data.Unlock()
		}
		time.Sleep(DelayBetweenDirectoryUpdate)
	}
}
