package directory

import (
	"../data"
	"bytes"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)

func TestDevice(configuration *Configuration,
	dvc *data.DeviceCell) {
	if dvc != nil {
		request, _ := http.NewRequest("PUT",
			"https://"+
				configuration.Hostname+
				":"+
				strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
				"/api/v1/directory/test?type="+
				device.CapabilityTypeName[dvc.Type]+
				"&name="+
				dvc.Name,
			nil)
		if response, err := common.InsecureHTTPClient.Do(request); err == nil {
			_, _ = io.Copy(ioutil.Discard,
				response.Body)
			_ = response.Body.Close()
		} else {
			fmt.Println("Test failed:",
				err)
		}
	}
}

func SendAction(configuration *Configuration,
	action *common.DirectoryAction) {
	request, _ := http.NewRequest("PUT",
		"https://"+
			configuration.Hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/action",
		bytes.NewBuffer(action.MarshalJson()))
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		_, _ = io.Copy(ioutil.Discard,
			response.Body)
		_ = response.Body.Close()
	} else {
		fmt.Println("Action send failed:",
			err)
	}
}
