package window

import (
	"../data"
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"gitlab.com/ghostbutler/tool/service"
	"golang.org/x/image/font/basicfont"
	"unicode"
)

const (
	DeviceOutletMargin = 30
)

type DeviceOutlet struct {
	// resources
	resource *data.Resource

	// directory action
	Action *common.DirectoryAction

	// currently selected
	CurrentDeviceCell *data.DeviceCell

	// on object
	OnRect     pixel.Rect
	OnPosition pixel.Vec

	// off object
	OffRect     pixel.Rect
	OffPosition pixel.Vec

	// text atlas
	TextAtlas *text.Atlas
}

func BuildDeviceOutlet(resource *data.Resource) View {
	return &DeviceOutlet{
		resource: resource,
		TextAtlas: text.NewAtlas(
			basicfont.Face7x13,
			text.ASCII,
			text.RangeTable(unicode.Latin),
		),
	}
}

func (deviceOutlet *DeviceOutlet) Update(window *pixelgl.Window,
	_ *data.Directory) bool {
	deviceOutlet.Action = nil

	if window.JustPressed(pixelgl.KeyEscape) ||
		window.JustPressed(pixelgl.MouseButton2) {
		return false
	}

	deviceOutlet.OnPosition = pixel.V(DeviceLightMargin+deviceOutlet.resource.Power[true].Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight/2)
	deviceOutlet.OffPosition = pixel.V(DeviceLightMargin+deviceOutlet.resource.Power[true].Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin-data.PowerImageHeight/2)

	if window.JustPressed(pixelgl.MouseButton1) {
		deviceOutlet.OnRect = pixel.R(DeviceLightMargin,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight,
			DeviceLightMargin+deviceOutlet.resource.Power[true].Picture().Bounds().Max.X,
			window.Bounds().Max.Y-DeviceLightMargin)
		deviceOutlet.OffRect = pixel.R(DeviceLightMargin,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin-data.PowerImageHeight,
			DeviceLightMargin+deviceOutlet.resource.Power[true].Picture().Bounds().Max.X,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin)

		if deviceOutlet.OnRect.Contains(window.MousePosition()) {
			deviceOutlet.Action = &common.DirectoryAction{
				Type:       "outlet",
				Identifier: "on",
				Name:       deviceOutlet.CurrentDeviceCell.Name,
			}
		}
		if deviceOutlet.OffRect.Contains(window.MousePosition()) {
			deviceOutlet.Action = &common.DirectoryAction{
				Type:       "outlet",
				Identifier: "off",
				Name:       deviceOutlet.CurrentDeviceCell.Name,
			}
		}
		// todo add name editing here
	}

	return true
}

func (deviceOutlet *DeviceOutlet) Draw(window *pixelgl.Window) {
	deviceOutlet.resource.Power[true].Draw(window,
		pixel.IM.Moved(deviceOutlet.OnPosition))
	deviceOutlet.resource.Power[false].Draw(window,
		pixel.IM.Moved(deviceOutlet.OffPosition))

	t := text.New(pixel.Vec{},
		deviceOutlet.TextAtlas)
	_, _ = fmt.Fprintln(t,
		deviceOutlet.CurrentDeviceCell.Name)
	t.Draw(window,
		pixel.IM.Scaled(t.Orig,
			SensorNameZoom).Moved(pixel.V(deviceOutlet.OffPosition.X-deviceOutlet.resource.Power[false].Picture().Bounds().Max.X/2,
			deviceOutlet.OffPosition.Y+data.PowerImageHeight/2-DeviceLightInterMargin-data.PowerImageHeight-t.LineHeight*SensorNameZoom)))
}

func (deviceOutlet *DeviceOutlet) GetAction() *common.DirectoryAction {
	return deviceOutlet.Action
}

func (deviceOutlet *DeviceOutlet) SetCell(cell *data.DeviceCell) {
	deviceOutlet.CurrentDeviceCell = cell
}
