package window

import (
	"../data"
	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/ghostbutler/tool/service"
)

type ViewType string

const (
	ViewTypeDevice       = ViewType("device")
	ViewTypeDeviceLight  = ViewType("deviceLight")
	ViewTypeDeviceOutlet = ViewType("deviceOutlet")
	ViewTypeDeviceMusic  = ViewType("deviceMusic")
	ViewTypeDeviceTTS    = ViewType("deviceTTS")
)

type View interface {
	Update(window *pixelgl.Window,
		directory *data.Directory) bool
	Draw(window *pixelgl.Window)
	GetAction() *common.DirectoryAction
	SetCell(cell *data.DeviceCell)
}
