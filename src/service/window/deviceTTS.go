package window

import (
	"../data"
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"golang.org/x/image/font/basicfont"
	"time"
	"unicode"
)

const (
	DelayBetweenCursorBlink = time.Millisecond * 500
)

type DeviceTTS struct {
	isCursorDisplayed          bool
	lastCursorStatusChangeTime time.Time

	resource *data.Resource

	CurrentSentence string

	CurrentDeviceCell *data.DeviceCell

	Action *common.DirectoryAction

	TextAtlas *text.Atlas
}

func BuildDeviceTTS(resource *data.Resource) View {
	output := &DeviceTTS{
		resource: resource,
		TextAtlas: text.NewAtlas(
			basicfont.Face7x13,
			text.ASCII,
			text.RangeTable(unicode.Latin),
		),
	}

	return output
}

func (deviceTTS *DeviceTTS) Update(window *pixelgl.Window,
	directory *data.Directory) bool {
	deviceTTS.Action = nil

	if window.JustPressed(pixelgl.KeyEscape) ||
		window.JustPressed(pixelgl.MouseButton2) {
		return false
	}

	if key := window.Typed(); key != "" {
		deviceTTS.CurrentSentence += key
	} else {
		if window.JustPressed(pixelgl.KeyBackspace) {
			if len(deviceTTS.CurrentSentence) > 0 {
				deviceTTS.CurrentSentence = deviceTTS.CurrentSentence[:len(deviceTTS.CurrentSentence)-1]
			}
		}
	}

	if len(deviceTTS.CurrentSentence) > 0 {
		if window.JustPressed(pixelgl.KeyEnter) {
			deviceTTS.Action = &common.DirectoryAction{
				Name:       deviceTTS.CurrentDeviceCell.Name,
				Identifier: "tts",
				Type:       device.CapabilityTypeName[device.CapabilityTypeTextToSpeech],
				FieldList: map[string]interface{}{
					"sentence": deviceTTS.CurrentSentence,
					"language": "fr",
				},
			}
			deviceTTS.CurrentSentence = ""
		}
	}

	if time.Now().Sub(deviceTTS.lastCursorStatusChangeTime) > DelayBetweenCursorBlink {
		deviceTTS.isCursorDisplayed = !deviceTTS.isCursorDisplayed
		deviceTTS.lastCursorStatusChangeTime = time.Now()
	}

	return true
}

func (deviceTTS *DeviceTTS) Draw(window *pixelgl.Window) {
	// build text
	t := text.New(pixel.Vec{},
		deviceTTS.TextAtlas)

	// fill text
	if len(deviceTTS.CurrentSentence) > 0 {
		_, _ = fmt.Fprintln(t,
			deviceTTS.CurrentSentence)
	} else {
		_, _ = fmt.Fprintln(t,
			"_")
	}

	// center
	position := pixel.Vec{
		X: window.Bounds().Max.X/2 - ((t.Bounds().Max.X-t.Bounds().Min.X)*SensorNameZoom)/2,
		Y: (window.Bounds().Max.Y-window.Bounds().Min.Y)/2 - ((t.Bounds().Max.Y + t.Bounds().Min.Y) * SensorNameZoom / 2),
	}

	if len(deviceTTS.CurrentSentence) > 0 ||
		deviceTTS.isCursorDisplayed {
		t.Draw(window,
			pixel.IM.Scaled(t.Orig,
				SensorNameZoom).Moved(position))
	}
}

func (deviceTTS *DeviceTTS) GetAction() *common.DirectoryAction {
	return deviceTTS.Action
}

func (deviceTTS *DeviceTTS) SetCell(cell *data.DeviceCell) {
	deviceTTS.CurrentDeviceCell = cell
}
