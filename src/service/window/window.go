package window

import (
	"../data"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"golang.org/x/image/colornames"
	"net/http"
)

type Window struct {
	IsRunning     bool
	window        *pixelgl.Window
	directoryData *data.Directory

	device       *Device
	deviceLight  View
	deviceOutlet View
	deviceTTS    View

	CurrentViewType ViewType
	CurrentView     View

	resource *data.Resource
}

func BuildWindow(directoryData *data.Directory,
	statikFS http.FileSystem) (*Window, error) {
	window := &Window{
		CurrentViewType: ViewTypeDevice,
		directoryData:   directoryData,
		IsRunning:       true,
	}
	if w, err := pixelgl.NewWindow(pixelgl.WindowConfig{
		Bounds: pixel.R(0,
			0,
			640,
			480),
		Resizable: true,
		Title:     "Ghost butler desktop app",
		VSync:     true,
	}); err == nil {
		window.window = w
	} else {
		return nil, err
	}

	if res, err := data.BuildResource(statikFS); err == nil {
		window.resource = res
	} else {
		return nil, err
	}

	window.device = BuildDevice(window.resource)
	window.deviceLight = BuildDeviceLight(window.resource)
	window.deviceOutlet = BuildDeviceOutlet(window.resource)
	window.deviceTTS = BuildDeviceTTS(window.resource)

	window.CurrentView = window.device

	return window, nil
}

func (window *Window) Update() {
	// update
	if !window.CurrentView.Update(window.window,
		window.directoryData) {
		window.CurrentViewType = ViewTypeDevice
		window.CurrentView = window.device
	}
	switch window.CurrentViewType {
	case ViewTypeDevice:
		if window.device.IsMustRunDeviceView {
			window.device.IsMustRunDeviceView = false
			if dvc := window.device.CurrentDeviceCell; dvc != nil {
				switch dvc.Type {
				case device.CapabilityTypeLight:
					window.deviceLight.SetCell(dvc)
					window.CurrentViewType = ViewTypeDeviceLight
					window.CurrentView = window.deviceLight
					break
				case device.CapabilityTypeOutlet:
					window.deviceOutlet.SetCell(dvc)
					window.CurrentViewType = ViewTypeDeviceOutlet
					window.CurrentView = window.deviceOutlet
					break
				case device.CapabilityTypeTextToSpeech:
					window.deviceTTS.SetCell(dvc)
					window.CurrentViewType = ViewTypeDeviceTTS
					window.CurrentView = window.deviceTTS
					break

				default:
					break
				}
			}
		}
		break
	}

	// draw
	window.window.Clear(colornames.Black)
	if window.window.Closed() {
		window.IsRunning = false
	}
	window.CurrentView.Draw(window.window)
	window.window.Update()
}

// get test device
func (window *Window) GetTestDevice() *data.DeviceCell {
	switch window.CurrentViewType {
	case ViewTypeDevice:
		if window.device.IsMustTestCurrentDevice {
			return window.device.CurrentDeviceCell
		} else {
			return nil
		}
	default:
		return nil
	}
}

// get action
func (window *Window) GetAction() *common.DirectoryAction {
	return window.CurrentView.GetAction()
}
