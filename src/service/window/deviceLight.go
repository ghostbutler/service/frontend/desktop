package window

import (
	"../data"
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"gitlab.com/ghostbutler/tool/service"
	"golang.org/x/image/font/basicfont"
	"strconv"
	"unicode"
)

const (
	DeviceLightMargin      = 30
	DeviceLightInterMargin = 5

	HueRatio = 1.411764
)

type DeviceLight struct {
	// resources
	resource *data.Resource

	// directory action
	Action *common.DirectoryAction

	// currently selected
	CurrentDeviceCell *data.DeviceCell

	// hue saturation object
	HueSaturationRect     pixel.Rect
	HueSaturationPosition pixel.Vec

	// brightness object
	BrightnessRect     pixel.Rect
	BrightnessPosition pixel.Vec

	// on object
	OnRect     pixel.Rect
	OnPosition pixel.Vec

	// off object
	OffRect     pixel.Rect
	OffPosition pixel.Vec

	// text atlas
	TextAtlas *text.Atlas
}

func BuildDeviceLight(resource *data.Resource) View {
	return &DeviceLight{
		resource: resource,
		TextAtlas: text.NewAtlas(
			basicfont.Face7x13,
			text.ASCII,
			text.RangeTable(unicode.Latin),
		),
	}
}

func (deviceLight *DeviceLight) Update(window *pixelgl.Window,
	_ *data.Directory) bool {
	deviceLight.Action = nil

	if window.JustPressed(pixelgl.KeyEscape) ||
		window.JustPressed(pixelgl.MouseButton2) {
		return false
	}

	deviceLight.HueSaturationPosition = pixel.V(DeviceLightMargin+deviceLight.resource.HueSaturation.Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-deviceLight.resource.HueSaturation.Picture().Bounds().Max.Y/2)
	deviceLight.BrightnessPosition = pixel.V(DeviceLightMargin+deviceLight.resource.Brightness.Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-deviceLight.resource.HueSaturation.Picture().Bounds().Max.Y-DeviceLightInterMargin-deviceLight.resource.Brightness.Picture().Bounds().Max.Y/2)
	deviceLight.OnPosition = pixel.V(DeviceLightMargin+deviceLight.resource.HueSaturation.Picture().Bounds().Max.X+DeviceLightMargin+deviceLight.resource.Power[true].Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight/2)
	deviceLight.OffPosition = pixel.V(DeviceLightMargin+deviceLight.resource.HueSaturation.Picture().Bounds().Max.X+DeviceLightMargin+deviceLight.resource.Power[false].Picture().Bounds().Max.X/2,
		window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin-data.PowerImageHeight/2)

	if window.JustPressed(pixelgl.MouseButton1) {
		deviceLight.HueSaturationRect = pixel.R(DeviceLightMargin,
			window.Bounds().Max.Y-DeviceLightMargin-deviceLight.resource.HueSaturation.Picture().Bounds().Max.Y,
			DeviceLightMargin+deviceLight.resource.HueSaturation.Picture().Bounds().Max.X,
			window.Bounds().Max.Y-DeviceLightMargin)
		deviceLight.BrightnessRect = pixel.R(DeviceLightMargin,
			deviceLight.HueSaturationRect.Min.Y-DeviceLightInterMargin-deviceLight.resource.Brightness.Picture().Bounds().Max.Y,
			DeviceLightMargin+deviceLight.resource.Brightness.Picture().Bounds().Max.X,
			deviceLight.HueSaturationRect.Min.Y-DeviceLightInterMargin)
		deviceLight.OnRect = pixel.R(deviceLight.HueSaturationRect.Max.X+DeviceLightMargin,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight,
			deviceLight.HueSaturationRect.Max.X+DeviceLightMargin+deviceLight.resource.Power[true].Picture().Bounds().Max.X,
			window.Bounds().Max.Y-DeviceLightMargin)
		deviceLight.OffRect = pixel.R(deviceLight.HueSaturationRect.Max.X+DeviceLightMargin,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin-data.PowerImageHeight,
			deviceLight.HueSaturationRect.Max.X+DeviceLightMargin+deviceLight.resource.Power[true].Picture().Bounds().Max.X,
			window.Bounds().Max.Y-DeviceLightMargin-data.PowerImageHeight-DeviceLightMargin)
		if deviceLight.HueSaturationRect.Contains(window.MousePosition()) {
			hue := int((window.MousePosition().X - deviceLight.HueSaturationRect.Min.X) * HueRatio)
			saturation := int(window.MousePosition().Y - deviceLight.HueSaturationRect.Min.Y)
			deviceLight.Action = &common.DirectoryAction{
				Type:       "light",
				Identifier: "color",
				Name:       deviceLight.CurrentDeviceCell.Name,
				FieldList: map[string]interface{}{
					"color": "hue=" +
						strconv.Itoa(hue) +
						"&saturation=" +
						strconv.Itoa(saturation),
				},
			}
		}
		if deviceLight.BrightnessRect.Contains(window.MousePosition()) {
			brightness := int(window.MousePosition().X - deviceLight.HueSaturationRect.Min.X)
			if brightness >= 255 {
				brightness = 255
			}
			deviceLight.Action = &common.DirectoryAction{
				Type:       "light",
				Identifier: "brightness",
				Name:       deviceLight.CurrentDeviceCell.Name,
				FieldList: map[string]interface{}{
					"brightness": brightness,
				},
			}
		}
		if deviceLight.OnRect.Contains(window.MousePosition()) {
			deviceLight.Action = &common.DirectoryAction{
				Type:       "light",
				Identifier: "on",
				Name:       deviceLight.CurrentDeviceCell.Name,
			}
		}
		if deviceLight.OffRect.Contains(window.MousePosition()) {
			deviceLight.Action = &common.DirectoryAction{
				Type:       "light",
				Identifier: "off",
				Name:       deviceLight.CurrentDeviceCell.Name,
			}
		}
		// todo add name editing here
	}

	return true
}

func (deviceLight *DeviceLight) Draw(window *pixelgl.Window) {
	deviceLight.resource.HueSaturation.Draw(window,
		pixel.IM.Moved(deviceLight.HueSaturationPosition))
	deviceLight.resource.Brightness.Draw(window,
		pixel.IM.Moved(deviceLight.BrightnessPosition))
	deviceLight.resource.Power[true].Draw(window,
		pixel.IM.Moved(deviceLight.OnPosition))
	deviceLight.resource.Power[false].Draw(window,
		pixel.IM.Moved(deviceLight.OffPosition))

	t := text.New(pixel.Vec{},
		deviceLight.TextAtlas)
	_, _ = fmt.Fprintln(t,
		deviceLight.CurrentDeviceCell.Name)
	t.Draw(window,
		pixel.IM.Scaled(t.Orig,
			SensorNameZoom).Moved(pixel.V(deviceLight.OffPosition.X-deviceLight.resource.Power[false].Picture().Bounds().Max.X/2,
			deviceLight.OffPosition.Y+data.PowerImageHeight/2-DeviceLightInterMargin-data.PowerImageHeight-t.LineHeight*SensorNameZoom)))
}

func (deviceLight *DeviceLight) GetAction() *common.DirectoryAction {
	return deviceLight.Action
}

func (deviceLight *DeviceLight) SetCell(cell *data.DeviceCell) {
	deviceLight.CurrentDeviceCell = cell
}
