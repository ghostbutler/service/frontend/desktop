package window

import (
	"../data"
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"gitlab.com/ghostbutler/tool/service"
	"golang.org/x/image/font/basicfont"
	"strings"
	"unicode"
)

const (
	SpaceBetweenDeviceIcon = 32

	SensorNameZoom = 2

	DeviceNameMarginBottom = 15
)

type Device struct {
	// display map
	DisplayMap map[data.Capability][]*data.DeviceCell

	// batch
	batch *pixel.Batch

	// resources
	resource *data.Resource

	// scroll
	Scroll pixel.Vec

	// currently selected
	CurrentDeviceCell       *data.DeviceCell
	IsMustTestCurrentDevice bool
	IsMustRunDeviceView     bool

	// text atlas
	TextAtlas *text.Atlas
}

func BuildDevice(resource *data.Resource) *Device {
	dvc := &Device{
		DisplayMap: make(map[data.Capability][]*data.DeviceCell),
		batch: pixel.NewBatch(&pixel.TrianglesData{},
			resource.DeviceIconPicture),
		resource: resource,
		TextAtlas: text.NewAtlas(
			basicfont.Face7x13,
			text.ASCII,
			text.RangeTable(unicode.Latin),
		),
	}
	return dvc
}

func (device *Device) Update(window *pixelgl.Window,
	directory *data.Directory) bool {
	newDisplayMap := make(map[data.Capability][]*data.DeviceCell)
	currentPosition := pixel.V(SpaceBetweenDeviceIcon,
		SpaceBetweenDeviceIcon)
	device.IsMustTestCurrentDevice = false
	device.IsMustRunDeviceView = false
	device.CurrentDeviceCell = nil
	for _, capability := range data.CapabilityList {
		if deviceList, ok := directory.DeviceList[data.CapabilityMap[capability]]; ok {
			newDisplayMap[capability] = make([]*data.DeviceCell, 0, 1)
			//isSomethingFound := false
			for deviceIndex := 0; deviceIndex < len(deviceList); deviceIndex++ {
				isDeviceAccepted := true
				switch capability {
				case data.CapabilityGamepadJoystick:
					if !strings.Contains(deviceList[deviceIndex],
						"joystick") {
						isDeviceAccepted = false
					}
					break
				case data.CapabilityGamepadButton:
					if strings.Contains(deviceList[deviceIndex],
						"joystick") {
						isDeviceAccepted = false
					}
					break

				default:
					break
				}

				if isDeviceAccepted {
					//isSomethingFound = true
					deviceDisplay := &data.DeviceCell{
						Name: deviceList[deviceIndex],
						IsActive: directory.IsDeviceActive(data.CapabilityMap[capability],
							deviceList[deviceIndex]),
						Position: pixel.V(currentPosition.X+data.DeviceIconHeight/2,
							window.Bounds().Max.Y-currentPosition.Y-data.DeviceIconHeight+data.DeviceIconHeight/2+device.Scroll.Y),
						Rect: pixel.R(currentPosition.X,
							window.Bounds().Max.Y-currentPosition.Y-data.DeviceIconHeight+device.Scroll.Y,
							currentPosition.X+data.DeviceIconWidth,
							window.Bounds().Max.Y-currentPosition.Y+device.Scroll.Y),
						Type: data.CapabilityMap[capability],
					}
					newDisplayMap[capability] = append(newDisplayMap[capability],
						deviceDisplay)

					if deviceDisplay.Rect.Contains(window.MousePosition()) {
						device.CurrentDeviceCell = deviceDisplay
						if window.JustPressed(pixelgl.MouseButton2) {
							device.IsMustTestCurrentDevice = true
						} else if window.JustPressed(pixelgl.MouseButton1) {
							device.IsMustRunDeviceView = true
						}
					}

					//if deviceIndex + 1 < len(deviceList) {
					if currentPosition.X+data.DeviceIconWidth*2+SpaceBetweenDeviceIcon < window.Bounds().Max.X {
						currentPosition.X += data.DeviceIconWidth + SpaceBetweenDeviceIcon
					} else {
						currentPosition.X = SpaceBetweenDeviceIcon
						currentPosition.Y += data.DeviceIconHeight + SpaceBetweenDeviceIcon
					}
					//}
				}
			}

			//if isSomethingFound {
			//	currentPosition.X = SpaceBetweenDeviceIcon
			//	currentPosition.Y += data.DeviceIconHeight + SpaceBetweenDeviceIcon
			//}

			device.DisplayMap = newDisplayMap
		}
	}

	device.Scroll.X += window.MouseScroll().X
	device.Scroll.Y += window.MouseScroll().Y * 10

	return true
}

func (device *Device) Draw(window *pixelgl.Window) {
	device.resource.Mouse.Draw(window,
		pixel.IM.Moved(pixel.V(window.Bounds().Max.X/2,
			window.Bounds().Max.Y+device.resource.Mouse.Picture().Bounds().Max.Y/2+device.Scroll.Y)))
	device.batch.Clear()
	for _, capability := range data.CapabilityList {
		if deviceList, ok := device.DisplayMap[capability]; ok {
			for _, deviceInstance := range deviceList {
				device.resource.DeviceIcon[capability][deviceInstance.IsActive].Draw(device.batch,
					pixel.IM.Moved(deviceInstance.Position))
			}
		}
	}
	device.batch.Draw(window)

	if device.CurrentDeviceCell != nil {
		// build text
		t := text.New(pixel.Vec{},
			device.TextAtlas)

		// active?
		var active string
		if device.CurrentDeviceCell.IsActive {
			active += " (Active)"
		}
		// fill text
		_, _ = fmt.Fprintln(t,
			device.CurrentDeviceCell.Name+
				active)

		// center
		position := pixel.Vec{
			X: window.Bounds().Max.X/2 - ((t.Bounds().Max.X-t.Bounds().Min.X)*SensorNameZoom)/2,
			Y: (t.Bounds().Max.Y+t.Bounds().Min.Y)*SensorNameZoom/2 + DeviceNameMarginBottom,
		}
		t.Draw(window,
			pixel.IM.Scaled(t.Orig,
				SensorNameZoom).Moved(position))
	}
}

func (device *Device) GetAction() *common.DirectoryAction {
	return nil
}

func (device *Device) SetCell(cell *data.DeviceCell) {
	device.CurrentDeviceCell = cell
}
