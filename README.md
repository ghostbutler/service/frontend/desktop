Desktop app
===========

Introduction
------------

This is ghost butler management desktop application

Dependencies
------------

```bash
sudo apt install libgl1-mesa-dev xorg-dev
go get github.com/rakyll/statik
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/service/frontend/desktop
